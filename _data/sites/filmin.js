module.exports = {
  name: "Filmin web", // optional, falls back to object key
  description: "Filmin web",
  options: {
    frequency: 60 * 23, // 24 hours
    freshChrome: "site",
  },
  urls: [
    "https://www.filmin.es/",
    "https://www.filmin.es/catalogo",
    "https://www.filmin.es/pelicula/west-side-story",
  ],
};
